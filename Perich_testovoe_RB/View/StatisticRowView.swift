//
//  StatisticRowView.swift
//  Perich_testovoe_RB
//
//  Created by Petar Perich on 3.2.24..
//

import SwiftUI

struct StatisticRowView: View {
    var statistic: Statistic
    var maxValue: Double
    
    var body: some View {
        HStack {
            GeometryReader { geometry in
                ZStack(alignment: .leading) {
                    
                    Rectangle()
                        .fill(Color.gray.opacity(0.3))
                        .frame(height: 10)
                    
                    
                    if statistic.value > 0 {
                        Rectangle()
                            .fill(statistic.color)
                            .frame(width: (CGFloat(statistic.value) / CGFloat(maxValue)) * geometry.size.width, height: 10)
                    }
                }
            }
            
            Text(statistic.name)
                .frame(width: 100, alignment: .leading)
                .foregroundColor(.gray)
            
            Spacer()
            
            Text(String(format: "%.2f", statistic.value))
                .bold()
                .frame(alignment: .trailing)
        }
        .frame(height: 20)
    }
}
