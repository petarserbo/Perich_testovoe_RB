//
//  View.swift
//  Perich_testovoe_RB
//
//  Created by Petar Perich on 31.1.24..
//

import SwiftUI

struct BettingStatisticsView: View {
    @ObservedObject var viewModel: BettingStatisticsViewModel
    
    var body: some View {
        ScrollView {
            
            VStack(spacing: 15){
                
                VStack(alignment: .leading, spacing: 8) {
                    Text("Средние коэффициенты")
                        .bold()
                        .padding(.bottom, 2)
                    ForEach(viewModel.statistics) { statistic in
                        StatisticRowView(statistic: statistic, maxValue: viewModel.maxValueForCoefficient)
                    }
                }
                .padding()
                .background(Color.white)
                .shadow(color: .black.opacity(0.1), radius: 4, x: 0, y: 2)
                
                
                BookmakerStatisticsRowView(viewModel: viewModel)
            }
            .padding(.horizontal)
            .padding(.top)
            
        }
        
    }
}

#Preview {
    BettingStatisticsView(viewModel: BettingStatisticsViewModel(statistics: [
        Statistic(name: "Выигрыш", value: 1.94, color: .green),
        Statistic(name: "Проигрыш", value: 2.17, color: .red),
        Statistic(name: "Возврат", value: 1.26, color: .gray)
    ], bookmaker: BookMaker()))
}
