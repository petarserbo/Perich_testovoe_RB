//
//  BookmakerStatisticsRowView.swift
//  Perich_testovoe_RB
//
//  Created by Petar Perich on 3.2.24..
//

import Foundation
import SwiftUI

struct BookmakerStatisticsRowView: View {
    let viewModel: BettingStatisticsViewModel
    
    
    init(viewModel: BettingStatisticsViewModel ){
        self.viewModel = viewModel
        
    }
    
    func doubleToInt(num: Double) -> Int{
        
        let integerPart = floor(num)
        let fractionalPart = num - integerPart
        
        if fractionalPart >= 0.5 {
            return Int(integerPart) + 1
        } else {
            return Int (integerPart)
        }
        
    }
    
    var body: some View {
        
        GeometryReader { geometry in
            
            VStack(alignment: .leading, spacing: 20) {
                Text("Выигрыши/проигрыши по букмекерам")
                    .font(.headline)
                    .padding([.top, .horizontal])
                
                HStack {
                    Image("Logo_1xBet")
                        .resizable()
                        .scaledToFit()
                        .frame(height: 30)
                    
                    Text("\(Int(viewModel.bookmaker.totalBets)) ставки")
                    
                        .bold()
                    
                }
                .padding([.horizontal])
                .padding(.bottom, 4)
                
                HStack (spacing: 2) {
                    
                    
                    
                    let spacing: CGFloat = 2 // Промежуток между барами
                    let padding: CGFloat = 16 // Отступы слева и справа
                    let totalPadding = padding * 2 // Общий отступ с обеих сторон
                    let totalSpacing = (2) * spacing // Общий промежуток между элементами
                    
                    let availableWidth = geometry.size.width - totalPadding - totalSpacing
                    
                    
                    
                    StatisticsBar(label: "\(viewModel.bookmaker.wins) (\(doubleToInt(num: viewModel.bookmaker.winPercentage))%)", color: .green, width: availableWidth * ((CGFloat(viewModel.bookmaker.wins)/CGFloat(viewModel.bookmaker.totalBets))), height: 30)
                    
                    StatisticsBar(label: "\(viewModel.bookmaker.losses) (\(doubleToInt(num: viewModel.bookmaker.losePercentage))%)", color: .red, width: availableWidth * ((CGFloat(viewModel.bookmaker.losses)/CGFloat(viewModel.bookmaker.totalBets))) , height: 30)
                    
                    StatisticsBar(label: "\(viewModel.bookmaker.returns) (\(doubleToInt(num: viewModel.bookmaker.returnPercentage))%)", color: .gray, width: availableWidth * ((CGFloat(viewModel.bookmaker.returns)/CGFloat(viewModel.bookmaker.totalBets))) , height: 30)
                    
                }
                
                .padding([.horizontal, .bottom])
                
            }
            
            .background(Color.white)
            .shadow(color: .black.opacity(0.1), radius: 4, x: 0, y: 2)
            
        }
        
    }
    
}


