//
//  StatisticsBar.swift
//  Perich_testovoe_RB
//
//  Created by Petar Perich on 31.1.24..
//

import SwiftUI

struct StatisticsBar: View {
    var label: String
    var color: Color
    var width: CGFloat
    var height: CGFloat
    
    var body: some View {
    
        VStack(alignment: .leading) {
            Rectangle()
                .fill(color)
            
            Text(label)
                .font(.caption)
                .bold()
                .lineLimit(1)
                .truncationMode(.tail)
            
        }
        
        .frame(width: width)
        .frame(height: height)
        
        
    }
}


