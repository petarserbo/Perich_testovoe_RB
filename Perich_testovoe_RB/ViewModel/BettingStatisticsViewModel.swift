//
//  BettingStatisticsViewModel.swift
//  Perich_testovoe_RB
//
//  Created by Petar Perich on 31.1.24..
//


import SwiftUI
import Combine

class BettingStatisticsViewModel: ObservableObject {

    @Published var statistics: [Statistic]
    @Published var bookmaker: BookMaker
    
    // Вычисляемое свойство для максимального коэффициента, используется для нормализации длины баров
    var maxValueForCoefficient: Double {
        statistics.max(by: { $0.value < $1.value })?.value ?? 1
    }
    
    init(statistics: [Statistic], bookmaker: BookMaker) {
        self.statistics = statistics
        self.bookmaker = bookmaker
        
    }
}




