//
//  Statistic.swift
//  Perich_testovoe_RB
//
//  Created by Petar Perich on 3.2.24..
//


import SwiftUI

struct Statistic: Identifiable {
    let id = UUID()
    let name: String
    let value: Double
    let color: Color
    
}
