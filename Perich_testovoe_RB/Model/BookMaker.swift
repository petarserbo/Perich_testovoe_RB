//
//  BookMaker.swift
//  Perich_testovoe_RB
//
//  Created by Petar Perich on 3.2.24..
//

import Foundation

struct BookMaker{
    var bookmakerName: String = "1xbet"
    var totalBets: Int = 32
    var wins: Int = 16
    var losses: Int = 11
    var returns: Int = 5
    
    // Вычисляемые свойства для процентов выигрышей, проигрышей и возвратов
    var winPercentage: Double {
        totalBets > 0 ? Double(wins) / Double(totalBets) * 100 : 0
    }
    
    var losePercentage: Double {
        totalBets > 0 ? Double(losses) / Double(totalBets) * 100 : 0
    }
    
    var returnPercentage: Double {
        totalBets > 0 ? Double(returns) / Double(totalBets) * 100 : 0
    }
    
}



