//
//  Perich_testovoe_RBApp.swift
//  Perich_testovoe_RB
//
//  Created by Petar Perich on 31.1.24..
//

import SwiftUI

@main
struct Perich_testovoe_RBApp: App {
    
    var body: some Scene {
        
        WindowGroup {
            
            BettingStatisticsView(viewModel: BettingStatisticsViewModel(statistics: [
                Statistic(name: "Выигрыш", value: 1.94, color: .green),
                Statistic(name: "Проигрыш", value: 2.17, color: .red),
                Statistic(name: "Возврат", value: 1.26, color: .gray)
            ], bookmaker: BookMaker()))
        }
    }
}

